// var script = document.createElement('script');
// script.src = 'https://itunes.apple.com/lookup?id=909253&entity=album&callback=loadAlbums';
// document.body.appendChild(script);

$.getJSON('https://itunes.apple.com/lookup?id=909253&entity=album&callback=?', loadAlbums);

var albumTemplateString = $('#album-template').html();
var albumTemplate = Handlebars.compile(albumTemplateString); // returns a function
var tracksTemplateString = $('#tracks-template').html();
var tracksTemplate = Handlebars.compile(tracksTemplateString);

function loadAlbums(response) {
  console.log(response);
  var albumHtmlList = '';

  response.results.forEach(function(album) {
    if (album.collectionType === 'Album') {
      albumHtmlList += albumTemplate(album);
    }
  });

  $('#albums').html(albumHtmlList);
}

function loadTracks(response) {
  console.log(response);
  var html = tracksTemplate({
    tracks: response.results
  });

  $('#tracks').html(html);
}

// event delegation
$('#albums').on('click', 'a', function(e) {
  e.preventDefault();
  $('#tracks').html('Loading...');
  var collectionId = $(this).data('collection-id');

  var script = document.createElement('script');
  var baseUrl = 'https://itunes.apple.com/lookup?id=';
  var url = baseUrl + collectionId + '&entity=song&callback=loadTracks';
  script.src = url;
  document.body.appendChild(script);
});
